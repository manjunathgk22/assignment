var application = angular.module('myApp', ['ngGrid']);
application.filter('myFilter', function() {

    return function(items, filterValue, search, filter) {
        var filtereditems = [];
        for (var i = 0; i < items.length; i++) {
            if (search) {
                if (items[i].schoolname.indexOf(filterValue) !== -1 || items[i].address.indexOf(filterValue) !== -1 || items[i].area.indexOf(filterValue) !== -1 || items[i].pincode === filterValue || items[i].landmark.indexOf(filterValue) !== -1) {
                    filtereditems.push(items[i])
                }
            }
            else if(filter){
            	if (items[i][filter].indexOf(filterValue) !== -1 ) {
                    filtereditems.push(items[i])
                }	
            }
        }


        return filtereditems;
    }
});
application.controller('myCtrl', [
    '$scope',
    '$http',
    '$filter',
    function($scope, $http, $filter) {
        /*fetching the data*/
        $scope.fetchData = function() {
            var urlString = "globalData.json";
            return $http({
                method: 'GET',
                url: urlString
            }).then(function success(response) {
                $scope.lastUpdate = new Date().toLocaleTimeString();
                return response.data;
            }, function error(response) {
                return "Failed to Fetch Data";
            })
        };
        /*initialisation*/
        this.$onInit = function() {
            $scope.Data = [],
                $scope.globalData = [],
                $scope.categoryList = [],
                $scope.mediumofInstList = [],
                $scope.genderList = [],
                $scope.filterList = ['filter','gender', 'category', 'medium_of_inst'];
            $scope.fetchData().then(function success(data) {
                $scope.globalData = angular.copy(data);

                for (var i = 0; i < data.length; i++) {
                    if (data[i].category && !$scope.categoryList.includes(data[i].category))
                        $scope.categoryList.push(data[i].category);
                    if (data[i].medium_of_inst && !$scope.mediumofInstList.includes(data[i].medium_of_inst))
                        $scope.mediumofInstList.push(data[i].medium_of_inst);
                    if (data[i].gender && !$scope.genderList.includes(data[i].gender))
                        $scope.genderList.push(data[i].gender);

                    delete data[i].district;
                    delete data[i].identification1;
                    delete data[i].identification2;
                    delete data[i].latlong;
                    $scope.Data.push(data[i])
                }
                $scope.data = angular.copy($scope.Data);


            });
        };

        $scope.$watchGroup(['filterValue','gender', 'category', 'mediumofInst'], function(newValues, oldValues, scope) {
        	$scope.data = $scope.Data
        	if(newValues[0] !== oldValues[0]){
        				$scope.data = $filter('myFilter')($scope.data, $scope.filterValue, true);
        			}
        	for(var i =1; i<newValues.length ; i++){
        		if(newValues[i] !== oldValues[i]){
        			
        				
        				$scope.data = $filter('myFilter')($scope.data, newValues[i], false, $scope.filterList[i]);
        			
        		}
        	}
            
        });
        $scope.ngGridView = {
            data: 'data',
            showGroupPanel: true,
            jqueryUIDraggable: true
        };

    }
]);